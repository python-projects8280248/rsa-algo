# RSA Algo

The project is an example of the implementation of the RSA encryption algorithm for message encryption.

## Application of an electronic signature

First of all, it is necessary to obtain the sender's public key, which will be used to verify the electronic digital signature. To do this, it is necessary to generate two random large primes of approximately a given size (pO, qO).

The algorithm of the basic method can be represented by the following steps: 
1. Set l to the lower limit of the range in which the prime number should be located. 
2. Set u the upper limit of the range in which the prime number should be located.
 3. Check the correctness of the range setting. 2<l≤u
4. Calculate the maximum number of attempts. r←100([〖log〗_2^  u]+1)
5.  	 r←r-1
6. If r<0, then the limit of attempts is exhausted (the event is very unlikely, and in practice almost never occurs. In the event that this does happen, you just need to restart the test). 
7. Select a random number n in a given interval 1≤n≤u. 8. If n is less than 2000, then testing is performed by trial division by all known primes that are less than n.
9. If n is more than 2000, then testing is performed by trial division by all primes that are less than more than 2000. If the divisor exists, then proceed to step 5.
10. At this step, you can start checking the number for simplicity using another method (which is not related to the partial division method). 
11. If the test result is negative (n turned out to be composite), proceed to step 5. 
12. If the test result is positive, then we generate a prime number, which is what we needed.

### Checking for simplicity
After generating the primes, you need to check them for simplicity.
The Miller-Rabin test was used in this section.:
1. We get an input of n >2, an odd natural number that needs to be checked for simplicity and k is the number of rounds.
1. Represent n - 1 as 2^s*t, where t is odd. This can be done by sequentially dividing n-1 by 2.
1. Cycle A: repeat k times:
    - Select a random integer a in the segment [2, n-2]
	- x = a^t mod n, calculated by exponentiation modulo
    - if x = 1 or x = n - 1, then go to the next iteration
    - cycle B: repeat s - 1 times: 
	    - x = x^2 mod n
        - if x = 1, then the number is composite
        - if x = n - 1, then go to the next iteration of the loop A
    - Return “composite”
1. Return “simple”

Then determine the value of tO = po * qO. In the next step, it is necessary to calculate the multiplicative arithmetic Euler function fO(𝑛O) = (𝑞O − 1)⋅ (𝑝O − 1). Next, to generate the sender's public key, you must select the number eO, which is in the range 1 < 𝑒O < fO(𝑛O), at which the condition NODE(eO, fO(𝑛O)) = 1 is fulfilled. As a result, the pair of numbers 𝑒O, 𝑛O will be the public key.

### Sender's private key

After receiving the public key, you must receive the sender's private key. To do this, using the extended Euclidean algorithm, we obtain the number dO corresponding to the conditions dO < by, eO * dO ≡ 1 (mod fO(nO)).As a result , a pair of numbers dO , nO will be a private key

### Recipient's public key

In the next step, you need to create the recipient's public key to encrypt the source text with it. To do this, you need to generate two random large prime numbers, approximately of a given size (rP, qp).

Next, you need to check the obtained numbers for simplicity.

Then determine the value of pP = rP * tp. In the next step, it is necessary to calculate the multiplicative arithmetic Euler function fP(𝑛N) = (𝑞N − 1)⋅ (𝑝P − 1). Next, you need to select the number of eP in the range 1 < 𝑒N < fP(𝑛N), at which the condition of the node(eP, fP(𝑛P)) = 1. As a result, a pair of numbers 𝑒N, 𝑛N will be the public key.

### Getting the recipient's private key

After creating the public key, the recipient's private key is received, with which the message will be decrypted. To do this, using the extended Euclidean algorithm, it is necessary to obtain the number dP corresponding to the conditions dP < pP, eP * dP ≡ 1 (mod fP(pP)).As a result, the resulting pair dP , pP will be the private key.. Thus, the number of pP will be a common component in both recipient keys.

### Key exchange

The next step is to send the recipient's public key to the sender. To do this, click the "Exchange keys" button on the recipient's on-screen form, after which the recipient's public key will appear on the sender's on-screen form.

### Encrypting, signing and sending the source text

We will encrypt and sign the source text before sending it. To do this, click on the "Encrypt and sign message" button on the sender's on-screen form. As a result, our text will first be divided into blocks, after which each block will be signed according to the formula s_i= M_i^(d_O ) (mod n_O), where Mi is the block of the source text. In the next step, each block will be sequentially encrypted using the formula m_i= s_i^(e_P ) (mod n_P), where si is a signed block of text.

After successful encryption and signing of the source text, we send it using the "Send message" button on the sender's on-screen form. As a result, the recipient receives an encrypted text along with the EDS, as well as the sender's public key to verify it.

### Decryption of the message

After receiving the message, first of all it is necessary to decrypt it in order to further verify the EDS. To do this, click the "verify" button on the recipient's on–screen form, then according to the formula s_i^ = m_i^(d_P ) (mod n_P), where mi is an encrypted block of text. decryption will be performed sequentially. Finally, you can check the EDS, for this you need to apply the formula M_i^ = s_i^(e_O) (mod n_O) to each block of signed text.

## Conclusion

In the course of this work, we studied the method of constructing and developing the RSA algorithm, created to create electronic digital signatures that should be used when sending messages over the network in order to establish the authenticity of the sender.

We found out that functionally the EDS guarantees that:
- The signed text comes from the person who signed it;
- The sender cannot waive the obligations associated with
the signed document;
- The signed text will be complete.

In particular, the RSA algorithm was used, which encrypts
the message creates a digital signature, attaches it to the message and sends it to the recipient along with some set of public keys. In
turn, the recipient needs to perform a number of similar
mathematical operations to establish the authenticity of the EDS and
decrypt the message.

The durability of the RSA digital signature algorithm is based on the factorization of the generated primes, from which the private key is composed. The verification of EDS in the RSA algorithm is based on the mathematical properties of prime numbers.
